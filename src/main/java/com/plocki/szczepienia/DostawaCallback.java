package com.plocki.szczepienia;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class DostawaCallback implements JavaDelegate {
	

	public void execute(DelegateExecution execution) throws Exception {
		RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();
		runtimeService.createMessageCorrelation("wynikDostawyMsg")
		  .processInstanceBusinessKey(execution.getVariable("parentBussinesKey").toString())
		  .setVariable("dostepnosc", execution.getVariable("dostepnosc"))
		  .correlateWithResult();
		
	}
}