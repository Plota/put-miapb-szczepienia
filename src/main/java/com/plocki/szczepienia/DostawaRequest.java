package com.plocki.szczepienia;

import java.util.HashMap;
import java.util.Map;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class DostawaRequest implements JavaDelegate{

public void execute(DelegateExecution execution) throws Exception {
		
		RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();
		Map<String, Object> processVariables = new HashMap<String, Object>();
		processVariables.put("parentBussinesKey",  execution.getProcessBusinessKey());
		processVariables.put("dostepnosc", execution.getVariable("dostepnosc"));
		
		runtimeService.startProcessInstanceByMessage("dostawaMsg", processVariables);
		
	}
}
